from django.shortcuts import render
from .forms import Add_ToDo
from .models import ToDo
from datetime import datetime
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

# Enter your name here
nama = "Hanna Jannatunna'iim"
aboutMe = '''Hi! I’m Hanna. I’m a person who likes to work hard and learn new things.
		I also easily adapt to the new environment. Creating and organizing events is fun for me.
		I do what i love and love what i do.'''
response = {'name': nama, 'about' : aboutMe}


# Create your views here.
def index(request):
	return render(request, 'profile_web.html', response)

def activities(request):
	toDoList = ToDo.objects.all().values()
	response['toDoList'] = convert_queryset_into_json(toDoList)
	response['form'] = Add_ToDo
	return render(request, 'project.html', response)

# method untuk membuat entry baru
def add_message(request):
	form = Add_ToDo(request.POST)
	if request.method == 'POST' and form.is_valid():
		print(request.POST)
		date_converted = datetime.strptime(request.POST['date'], '%Y-%m-%d')
		time_converted = datetime.strptime(request.POST['time'], '%H:%M')
		message = ToDo(date=date_converted, time=time_converted,categories=request.POST['categories'], activities=request.POST['activities'], place=request.POST['place'])
		message.save()
		
	return HttpResponseRedirect('/story4/activities/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def delete_todo(request, id=None):
	todoObject = get_object_or_404(ToDo, pk=id)
	todoObject.delete()
	return HttpResponseRedirect('/story4/activities/')
