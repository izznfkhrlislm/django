from django.db import models

# Create your models here.
class ToDo(models.Model):
	date = models.DateField()
	time = models.TimeField()
	categories = models.TextField(null=True)
	activities = models.TextField()
	place = models.TextField(null=True)

